<?php
/**
 * Plugin Name: TTN Elkarrizketak
 * Description: Azken Elkarrizketak
 * Version: 0.0.5
 * Author: Jimakker
 * Author URI: http://twitter.com/jimakker
 */


add_action( 'widgets_init', 'ttn_elkarrizketak' );


function ttn_elkarrizketak() {
	register_widget( 'Ttn_Elkarrizketak' );
}

class Ttn_Elkarrizketak extends WP_Widget {

	function __construct() {
		$widget_ops = array( 'classname' => 'ttn-elkarrizketak', 'description' => __('Azken Elkarrizketak', 'ttn-elkarrizketak') );

		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ttn-elkarrizketak-widget' );

		parent::__construct( 'ttn-elkarrizketak-widget', __('TTN Elkarrizketak', 'ttn-elkarrizketak'), $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );

		//Our variables from the widget settings.

		echo $before_widget;

ttn_elkarrizketak_output_fn();


		?>




		<?php
		echo $after_widget;
	}

	//Update the widget

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;


		return $instance;
	}


	function form( $instance ) {

		//Set up some default widget settings.
		$defaults = array( 'title' => __('TTN Elkarrizketak', 'example'), 'limit' => __('10', 'example'), 'show_info' => true );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>



	<?php
	}
}


function ttn_elkarrizketak_output_fn(){
?>
    <h3 style="background:#222;width:100%;color:#fff;text-align:center;margin:0;">ELKARRIZKETAK</h3>
		<div class="panel panel-default" style="margin-top:0px;border-top:0;border:0;border-radius:0;">

			<div class="list-group" style="max-height:300px;overflow-y:scroll;">
			<?php
			$args = array( 'post_type' => 'post', 'posts_per_page' => 10, 'cat' => 9 );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post();
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()),'thumbnail');
			?>
				<a title="<?php echo get_the_title();?>" href="<?php echo get_permalink();?>" class="list-group-item" style="padding:5px;min-height:50px;border-radius:0;color:#fff;background-color:#555;border-color:#333;">
					 <img class="media-object pull-left" style="margin-right:10px;margin-bottom:5px;" width="35" src="<?php echo $thumb[0];?>" alt="<?php the_title();?>">
					<?php the_title();?> <span class="meta text-muted"><time datetime="<?php echo the_time('Y-m-j'); ?>" class="published updated" pubdate><?php the_time('Y-m-j'); ?></time></span>
				</a>

				<?php
			endwhile;
			wp_reset_query();
			?>
			</div>
		</div>
 <?php
}

?>
